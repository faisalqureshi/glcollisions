#include <GLUT/glut.h>
#include <OPENGL/glu.h>
#include "World.h"
#include "Disk.h"

Eigen::Vector2i window_size;
Eigen::Vector4f view_area;

World w;

void display(void) {
  
  //clear white, draw with black
  glClearColor(255, 255, 255, 0);
  glColor3d(0, 0, 0);
  
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
  w.render();
  
  glutSwapBuffers();
  
}

void reshape(int width, int height) {
  window_size[0] = width;
  window_size[1] = height;
  
  if (height == 0) height = 1.0;
  
  // set viewport to window dimensions
  glViewport(0, 0, width, height);
  
  // reset projection system
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  
  // establish clipping volume
  GLfloat aspect_ratio, near = 1., far = -1.;
  aspect_ratio = (GLfloat) width / (GLfloat) height;
  if (width <= height) {
    view_area[0] = -100.;
    view_area[1] = 100.;
    view_area[2] = -100. / aspect_ratio;
    view_area[3] = 100. / aspect_ratio;
  }
  else {
    view_area[0] = -100. * aspect_ratio;
    view_area[1] = 100. * aspect_ratio;
    view_area[2] = -100.;
    view_area[3] = 100.;
  }
  glOrtho(view_area[0], view_area[1], view_area[2], view_area[3], near, far);

  // tell the world to update itself
  w.update_view_area(view_area);
  
  // set modelview matrix
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void idle(void) {
  double dt = 0.1;
  w.forward_simulation(dt);
  
  glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y) {
  switch( key ) {
    case 'q':
      exit(0);
    default:
      break;
  }
}

void setup_rc(void) {
  glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
}

int main(int argc, char **argv) {
  
  srand(0);
  
  // important, populate the world with disks
  w.populate(4, Eigen::Vector4f(-100, 100, -100, 100));
  //w.add_disk(new Disk(30,0,0,-1));
  //w.add_disk(new Disk(70,0,0,-1));
  
  // a basic set up...
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(640, 480);
  
  // create the window, the argument is the title
  glutCreateWindow("GLUT program");

  // pass the callbacks
  glutDisplayFunc(display);
  glutReshapeFunc(reshape);
  glutIdleFunc(idle);
  glutKeyboardFunc(keyboard);
  
  setup_rc();
  glutMainLoop();
  
  // we never get here because glutMainLoop() is an infinite loop
  return 0;
  
}
