#ifndef _Disk_H_
#define _Disk_H_

#include <iostream>
#include <Eigen/Core>
#include <vector>

class DiskState {
public:
  Eigen::Vector3d s[2];

  DiskState& operator=(const DiskState& o);
  Eigen::Vector3d& p() { return s[0]; }
  Eigen::Vector3d& v() { return s[1]; }
};

class Disk {
public:
  DiskState state;
  DiskState state_prev;
  double radius;
  double mass;
  Eigen::Vector3f color;
  bool unstep_available;
  Eigen::Vector4f wall_distances;
  bool disk_collision_handled;
  
	Disk();
  Disk(const Eigen::Vector4f& view_area);
  Disk(const DiskState& state);
	Disk(double x, double y, double vx, double vy);
  virtual ~Disk();
  
  void init();
  void render();
  void step(double dt, const Eigen::Vector4f& view_area);
  void unstep();
  void update_wall_distances(const Eigen::Vector4f& view_area);
  void handle_collision_with_wall(int j);
  int handle_collision_with_disk(Disk& o);
  bool check_disk_collision(Disk& o);
  bool check_wall_collision(int j);
  
//  void respond_to_collision_with_walls(const Eigen::Vector4f& view_area);
//  
//  void check_collision_disks(const std::vector<Disk*>& disks);
  
  //std::ostream& operator<<

};

#endif