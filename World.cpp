//
//  World.cpp
//  glcollisions
//
//  Created by Faisal Qureshi on 2/16/2014.
//
//

#include "World.h"
#include "Disk.h"

using namespace std;
using namespace Eigen;

World::~World()
{
  vector<Disk*>::iterator i;
  for (i=disks.begin(); i!=disks.end(); ++i) {
    Disk* d = (Disk*) *i;
    delete d;
  }
}

World::World()
{
  area = Eigen::Vector4f(-1., 1., -1., 1.);
  time_cur = time_prev = 0.0;
  initializing = true;
  initializing_time = 100;
}

void World::update_view_area(const Eigen::Vector4f& view_area)
{
  area = view_area;
}

void World::populate(int n, const Eigen::Vector4f &view_area)
{
  area = view_area;
  for (int i=0; i<n; ++i) {
    add_disk();
  }
}

void World::add_disk()
{
  disks.push_back(new Disk(area));
}

void World::add_disk(Disk* d)
{
  disks.push_back(d);
}

void World::step(double dt)
{
  time_prev = time_cur;
  
  vector<Disk*>::iterator i;
  for (i=disks.begin(); i!=disks.end(); ++i) {
    Disk* d = (Disk*) *i;
    d->step(dt, area);
  }
  
  time_cur += dt;

  cout << "time = " << time_cur << endl;
  
  print_disk_states();
}

void World::unstep()
{
  time_cur = time_prev;

  vector<Disk*>::iterator i;
  for (i=disks.begin(); i!=disks.end(); ++i) {
    Disk* d = (Disk*) *i;
    d->unstep();
  }
}

void World::render()
{
  vector<Disk*>::iterator i;
  for (i=disks.begin(); i!=disks.end(); ++i) {
    Disk* d = (Disk*) *i;
    d->render();
  }
}

void World::print_disk_states()
{
  for (int i=0; i<disks.size(); ++i) {
    Eigen::Vector3d& p = disks[i]->state.p();
    Eigen::Vector3d& v = disks[i]->state.v();
  
    cout << "====================\n";
    cout << "Disk " << i << endl;
    cout << p << endl;
    cout << v << endl;
    cout << "====================\n";
  }
}

void World::print_collision_distances(vector<collision_distance>& cdv)
{
  int i, n = cdv.size();
  for (i=0; i<n; ++i) {
    cout << cdv[i].d << " " << cdv[i].i << " " << cdv[i].j << " " << cdv[i].type << "\n";
  }
}

void World::forward_simulation(double time_step)
{
  this->time_step = time_step;
  step(time_step);
  
  compute_collision_distances(distances);
  double smallest_distance;
  //find_smallest_distance(distances, smallest_distance);
  find_next_collision(distances, smallest_distance);
  cout << "smallest distance " << smallest_distance << endl;

  if (smallest_distance < 0.0) {
    double collision_tolerance = 0.01;
    cout << "collision detected!! fixing.\n";

    if (!initializing) {
      find_time_of_collision(collision_tolerance, smallest_distance);
      cout << "\t time " << this->time_cur << ", smallest_distance " << smallest_distance << endl;
      cout << "fixed" << endl;
    
      compute_collision_response(collision_tolerance, distances);
    }
  }
  else {
    if (initializing) {
      initializing = false;
      cout << "Achieve steady state. Go.\n";
    }
    cout << "no collision detected.\n";
  }

  if (initializing && initializing_time < time_cur) {
    cout << "Cannot achieve steady state. Exiting.\n";
    exit(0);
  }
}

void World::compute_collision_distances(vector<collision_distance>& distances)
{
  distances.clear();
  
  int n = disks.size();
  int i;
  for (i=0; i<n; ++i) {
    disks[i]->update_wall_distances(area);
    const Vector4f& wd = disks[i]->wall_distances;
    
    distances.push_back(collision_distance(wd[0], i, 0, wall));
    distances.push_back(collision_distance(wd[1], i, 1, wall));
    distances.push_back(collision_distance(wd[2], i, 2, wall));
    distances.push_back(collision_distance(wd[3], i, 3, wall));
  }

  int j;
  for (i=0; i<n; ++i) {
    for (j=0; j<n; ++j) {
      if (j <= i) continue;
      
      Eigen::Vector3d& pi = disks[i]->state.p();
      Eigen::Vector3d& pj = disks[j]->state.p();
      double ri = disks[i]->radius;
      double rj = disks[j]->radius;
      Eigen::Vector3d ij = pi-pj;
      distances.push_back(collision_distance(ij.norm()-ri-rj, i, j, disk));
    }
  }
  
  print_collision_distances(distances);
}

int World::find_next_collision(vector<collision_distance>& distances, double& smallest_distance)
{
  smallest_distance = INFINITY;
  int smallest_distance_i = -1;

  int i, n = distances.size();
  for (i=0; i<n; ++i) {
    if (distances[i].d < 0.0) {
      // Ok, so there is a penetration.  We now have to check whether there
      // is a collision, i.e., the relative velocity will induce further penetration.

      // Check walls first
      if (distances[i].type == wall) {
        distances[i].coll = disks[distances[i].i]->check_wall_collision(distances[i].j);
      }
      
      // Now check disk-disk collisions
      else if (distances[i].type == disk) {
        distances[i].coll = disks[distances[i].i]->check_disk_collision(*disks[distances[i].j]);
      }
      
      if (distances[i].coll && distances[i].d < smallest_distance) {
        smallest_distance = distances[i].d;
        smallest_distance_i = i;
      }
    }
  }
  
  return smallest_distance_i;
}

int World::find_smallest_distance(vector<collision_distance>& distances, double& smallest_distance)
{
  smallest_distance = INFINITY;
  int smallest_distance_i = -1;
  
  int i, n = distances.size();
  for (i=0; i<n; ++i) {
    if (distances[i].d < smallest_distance) {
      smallest_distance = distances[i].d;
      smallest_distance_i = i;
    }
  }
  return smallest_distance_i;
}

void World::find_time_of_collision(double tolerance, double& smallest_distance)
{
  if (abs(smallest_distance) < tolerance) return;
  
  unstep();

  compute_collision_distances(distances);
  //find_smallest_distance(distances, smallest_distance);
  find_next_collision(distances, smallest_distance);
  
  int nIter = 10;
  double dt = this->time_step;
  while (abs(smallest_distance) > tolerance && nIter > 0) {
    cout << "\ttime " << this->time_cur << ", smallest_distance " << smallest_distance << ", iter " << nIter << endl;
    
    dt *= .5;
    if (smallest_distance < 0.0) {
      unstep();
      step(dt);
    }
    else {
      step(dt);
    }

    compute_collision_distances(distances);
    //find_smallest_distance(distances, smallest_distance);
    find_next_collision(distances, smallest_distance);
    
    --nIter;
  }
}

void World::compute_collision_response(double tolerance, vector<collision_distance>& distances)
{
  int i, n = distances.size();
  for (i=0; i<n; ++i) {
    collision_distance& cd = distances[i];
    
    if ( abs(cd.d) > tolerance ) continue;
    
    if ( cd.type == wall ) {
      cout << "Disk " << cd.i << " colliding with wall " << cd.j << endl;
      cout << "Pre collision velocity" << disks[cd.i]->state.v() << endl;
      disks[cd.i]->handle_collision_with_wall(cd.j);
      cout << "Post collision velocity" << disks[cd.i]->state.v() << endl;
    }
    
    if ( cd.type == disk ) {
      // 1) We can only handle collision between two disks
      // So if disks a and b have collided, we cannot handle
      // collision between  b and c at this instant
      //
      // 2) We assume that disk-wall collisions have already been
      // responded too at this stage.  This is a valid assumption
      // since the early distances include distances from the
      // wall.
      int r = disks[cd.i]->handle_collision_with_disk(*disks[cd.j]);
      if (r == -1) {
        cout << "Disk collision (" << cd.i << "," << cd.j << "): already handled\n";
      }
      else if (r == -2) {
        cout << "Disk collision (" << cd.i << "," << cd.j << "): only touching\n";
      }
      else {
        cout << "Disk collision (" << cd.i << "," << cd.j << "): handled\n";
      }
    }
    
  }
  
}

//void World::check_collisions_with_walls()
//{
//  vector<collision_distance> distances;
//  get_distances_from_walls(distances);
//  print_collision_distance(distances);
//
//  double smallest_negative, smallest_positive;
//  int i_smallest_negative, i_smallest_positive;
//  get_smallest(distances,
//               smallest_positive,
//               smallest_negative,
//               i_smallest_positive,
//               i_smallest_negative);
//  cout << smallest_positive << " " << smallest_negative << "\n";
//
//  if (smallest_negative < 0.0) {
//    goto_collision_instance(smallest_negative);
//    respond_to_collision_with_walls();
//  }
//}
//
//void World::get_distances_from_walls(vector<collision_distance>& distances)
//{
//  int i, side, n = disks.size();
//  for (i=0; i<n; ++i) {
//    double d = disks[i]->get_distance_from_walls(area, side);
//    collision_distance c_d(d, i, side, wall);
//    distances.push_back(c_d);
//  }
//}
//
//void World::get_smallest(vector<collision_distance>& distances,
//                         double& smallest_positive,
//                         double& smallest_negative,
//                         int& i_smallest_positive,
//                         int& i_smallest_negative)
//{
//  smallest_negative = 0.0;
//  smallest_positive = INFINITY;
//  i_smallest_positive = -1;
//  i_smallest_negative = -1;
//  for (int i=0; i<distances.size(); ++i) {
//    if (distances[i].d < 0 && smallest_negative > distances[i].d) {
//      smallest_negative = distances[i].d;
//      i_smallest_negative = i;
//    }
//    if (distances[i].d > 0 && smallest_positive > distances[i].d) {
//      smallest_positive = distances[i].d;
//      i_smallest_positive = i;
//    }
//  }
//}
//
//void World::goto_collision_instance(double largest_penetration)
//{
//  cout << "goto_collision_instance" << endl;
//  
//  double cur_err = largest_penetration;
//  double accuracy = 1e-3;
//  double dt = time_step;
//  vector<collision_distance> distances;
//  double smallest_positive, smallest_negative;
//  int i_smallest_positive, i_smallest_negative;
//  
//  
//  int iter = 0;
//  while (abs(cur_err) > accuracy || iter > 100) {
//    
//    if (cur_err < 0.0) { // Collision has occurred
//      unstep();
//      dt *= .5;
//      step(dt);
//      get_distances_from_walls(distances);
//      get_smallest(distances,
//                   smallest_positive,
//                   smallest_negative,
//                   i_smallest_positive,
//                   i_smallest_negative);
//      
//      if (smallest_negative >= 0.0) {
//        cur_err = smallest_positive;
//      }
//      else {
//        cur_err = smallest_negative;
//      }
//    }
//    else { // Collision hasn't occured yet
//      dt *= .5;
//      step(dt);
//      get_distances_from_walls(distances);
//      get_smallest(distances,
//                   smallest_positive,
//                   smallest_negative,
//                   i_smallest_positive,
//                   i_smallest_negative);
//      
//      if (smallest_negative >= 0.0) {
//        cur_err = smallest_positive;
//      }
//      else {
//        cur_err = smallest_negative;
//      }
//    }
//
//    ++iter;
//  }
//  
//  cout << "accuracy = " << cur_err << endl;
//}
//
//void World::respond_to_collision_with_walls()
//{
//  vector<Disk*>::iterator i;
//  for (i=disks.begin(); i!=disks.end(); ++i) {
//    Disk* d = (Disk*) *i;
//    d->respond_to_collision_with_walls(area);
//  }
//}
//
//
////void World::get_inter_disk_collision(vector<collision_distance>& distances)
////{
////  int i, j, n = disks.size();
////  
////  for (i=0; i<n; ++i) {
////    for (j=0; j<n; ++j) {
////      if (i == j) continue;
////      
////      Eigen::Vector3d& pi = disks[i]->state.p();
////      Eigen::Vector3d& pj = disks[j]->state.p();
////      double ri = disks[i]->radius;
////      double rj = disks[j]->radius;
////      
////      Eigen::Vector3d ij = pi - pj;
////      double d = ij.norm() - ri - rj;
////      //cout << "norm ij = " << ij.norm() << endl;
////      
////      collision_distance c_d(d, i, j, disk);
////      distances.push_back(c_d);
////      
//////      if (d < 0.0) {
//////        //cout << "Disks " << i << " and " << j << " collided." << endl;
//////        disk_distance dij;
//////        dij.d = d;
//////        dij.i = i;
//////        dij.j = j;
//////        d_distances.push_back(dij);
//////      }
////    }
////  }
////}
