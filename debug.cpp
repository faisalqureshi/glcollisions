/**
	@file debug.h
	@brief This file provides common debugging macros

  Author: 
  Faisal Zubair
  faisal@cs.toronto.edu

  DATE:
  $Date: 2004/09/07 17:19:29 $

  REVISION:
  $Id: debug.cpp,v 1.1.1.1 2004/09/07 17:19:29 faisal Exp $


	NOTES:

	 To use dbg levels. define _DBGLEVELS. Then use
   void SetDebugLevel(int), and int GetDebugLevel() to set the desired debug level. Default
   debug level is 0

	 Also you need to define _CONSOLE if you are doing CONSOLE programming
 

   Common debugging macros.  Many of these macros are similar to those provided
   by MFC and are designed to allow for "MFC neutral" code (code than can be
   compiled with or without MFC support).  Other macros add even more debugging
   facilities.
 
   Description of available macros:
 
   ASSERT			: Identical to the MFC macro of the same name.
   VERIFY			: Identical to the MFC macro of the same name.
   TRACE			: Identical to the MFC macro of the same name.
   INFO				: Similar to TRACE but includes a preamble specifying the
 					      file name and line number where the INFO is called as
 					      well as forces a line break at the end.
   TRACEL			: Identical to the MFC macro of the same name. (use with dbg_level)
   INFOL			: Similar to TRACE but includes a preamble specifying the
 					      file name and line number where the INFO is called as
 					      well as forces a line break at the end. (dbg_level)
	 TRACEOBJ   : Prints class.
	 INFOOBJ    : Prints class. Similar to INFO.
	 TRACEOBJL  : Prints class. (use with dbg_level)
	 INFOOBJL   : Prints class. Similar to INFO. (use with dbg_level)
   DBG				: Code contained within this macro is included only in
 					      _DEBUG builds.
   BREAK			: Forces a break in _DEBUG builds via DebugBreak.
   DECL_DOG_TAG		: Declares a "dog tag" within a class definition (see the
 					          discussion on dog tags below) in _DEBUG builds.
   CHECK_DOG_TAG	: Checks the validity of a "dog tag" in _DEBUG builds.
 
 
   Dog Tags
 
   Dog tags are a technique that can be used to verify that an object has
   not been stepped on by a memory overrun.  To use them you simply use the
   DECL_DOG_TAG macro to declare a dog tag at the beginning and ending of
   the class definition.  After this, you can check the validity of an
   object by using the CHECK_DOG_TAG macro.  For example:
 
   class MyClass
   {
   public:
      DECL_DOG_TAG(tagBegin);
      // code removed for brevity...
      DECL_DOG_TAG(tagEnd);
   };
 
	 and later:
 
   MyClass obj;
   // code removed for brevity...
   CHECK_DOG_TAG(obj.tagBegin);
   CHECK_DOG_TAG(obj.tagEnd);
 
*/

#include "debug.h"

// Make level debugging possible.
#if defined (_DBGLEVELS) && defined(_DEBUG) // _DBGLEVELS && _DEBUG

int dbg_level = 0;

void SetDebugLevel(int level)
{
  dbg_level = level;
  TRACE("Debug level set to %d.\n", level);
}

int GetDebugLevel()
{
  return dbg_level;
}

#endif // // _DBGLEVELS && _DEBUG

