#include "Disk.h"
#include <OPENGL/glu.h>

using namespace Eigen;

DiskState& DiskState::operator=(const DiskState& o)
{
  s[0] = o.s[0];
  s[1] = o.s[1];
  return *this;
}

void Disk::init()
{
  unstep_available = false;
  radius = 10.;
  mass = 1.;
  color = Vector3f::Random();

  state.v() = Vector3d::Random() * 25.;
  state.v()[2] = 0.0; // 2d simulation
}

Disk::Disk()
{
  init();
  state.p() = Vector3d::Random() * 50.;
  state.p()[2] = 0.0; // 2d simulation
}

Disk::Disk(const Eigen::Vector4f& view_area)
{
  init();

  Eigen::Vector3d& p = state.p();
  p = Vector3d::Random(); // between -1 and 1
                          // y = (x - x1) * (y2 - y1) / (x2 - x1) + y1;
  p[0] = (p[0] - (-1)) * (view_area[1] - view_area[0]) / (1 - (-1)) + view_area[0];
  p[1] = (p[1] - (-1)) * (view_area[3] - view_area[2]) / (1 - (-1)) + view_area[2];
  p[2] = 0.0; // 2d simulation
}

Disk::Disk(const DiskState& state)
{
  init();
  this->state = state;
}

Disk::Disk(double x, double y, double vx, double vy)
{
  init();
  state.p() = Eigen::Vector3d(x,y,0.);
  state.v() = Eigen::Vector3d(vx,vy,0.);
}


Disk::~Disk() {}

void Disk::render()
{
  Eigen::Vector3d& p = state.p();
  
  GLUquadricObj *pObj;
  glPushMatrix();
  glTranslatef(p[0], p[1], 0.0f); // not considering the z component
  pObj = gluNewQuadric();
  gluQuadricNormals(pObj, GLU_SMOOTH);
  glColor3f(color[0], color[1], color[2]);
  gluDisk(pObj, 0.0f, radius, 26, 13);
  glPopMatrix();
  
  gluDeleteQuadric(pObj);
}

void Disk::step(double dt, const Eigen::Vector4f& view_area)
{
  disk_collision_handled = false;
  
  state_prev = state;
  
  Eigen::Vector3d& p = state.p();
  Eigen::Vector3d& v = state.v();
  
  // Euler integration
  Vector3d p_next = p + v * dt;
  p = p_next;
  
//  bool spherical_world = false;
//  if (spherical_world) {
//    //std::cout << view_area << std::endl;
//    float w = view_area[1]-view_area[0];
//    float h = view_area[3]-view_area[2];
//    
//    if (p[0] < view_area[0]) p[0] += w;
//    else if (p[0] > view_area[1]) p[0] -= w;
//    
//    if (p[1] < view_area[2]) p[1] += h;
//    else if (p[1] > view_area[3]) p[1] -= h;
//  }
  
  // state_prev contains state at previous time
  // state contains the current state
  
  unstep_available = true;
}

void Disk::unstep()
{
  assert(unstep_available);
  state = state_prev;
  unstep_available = false;
}

void Disk::update_wall_distances(const Eigen::Vector4f& view_area)
{
  // Get current state
  const Eigen::Vector3d& p = state.p();
  const Eigen::Vector3d& v = state.v();

  // Current location
  const double& x = p[0];
  const double& y = p[1];
  
  // Penetrations w.r.t. the four walls
  wall_distances[0] = x - radius - view_area[0]; // dl
  wall_distances[1] = view_area[1] - x - radius; // dr
  wall_distances[2] = y - radius - view_area[2]; // db
  wall_distances[3] = view_area[3] - y - radius; // dt
}

void Disk::handle_collision_with_wall(int j)
{
  Eigen::Vector3d v = state.v();
  
  switch (j) {
    case 0:
      if (v[0] < 0) v[0] = -v[0];
      break;
    case 1:
      if (v[0] > 0) v[0] = -v[0];
      break;
    case 2:
      if (v[1] < 0) v[1] = -v[1];
      break;
    case 3:
      if (v[1] > 0) v[1] = -v[1];
      break;
  }
  state.v() = v;
}

bool Disk::check_wall_collision(int j)
{
  const Eigen::Vector3d& v = state.v();
    
    switch (j) {
      case 0:
        if (v[0] < 0) return true;
        break;
      case 1:
        if (v[0] > 0) return true;
        break;
      case 2:
        if (v[1] < 0) return true;
        break;
      case 3:
        if (v[1] > 0) return true;
        break;
    }
  return false;
}

bool Disk::check_disk_collision(Disk& o)
{
  DiskState& a = this->state;
  DiskState& b = o.state;
  Eigen::Vector3d n = (a.p() - b.p()).normalized();
  Eigen::Vector3d vab = a.v() - b.v();

  double d = vab.dot(n);
  if (d > 0) {
    return false; // Touching but no collision
  }
  return true;
}

int Disk::handle_collision_with_disk(Disk& o)
{
  if (disk_collision_handled) {
    return -1; // A disk collision has already been handled at this instance
  }

  disk_collision_handled = o.disk_collision_handled = true;
  
  DiskState& a = this->state;
  DiskState& b = o.state;
  double& ma = mass;
  double& mb = o.mass;
  
  Eigen::Vector3d n = (a.p() - b.p()).normalized();
  Eigen::Vector3d vab = a.v() - b.v();

  
  double d = vab.dot(n);
  if (d > 0) {
    return -2; // Touching but no collision
  }

  double e = 1.0;
  double j = - d * (1. + e) / ((1./ma) + (1./mb));
  
  a.v() = a.v() + n*j/ma;
  b.v() = b.v() - n*j/mb;
  
  return 0;
}

//
//  
////  double smallest_negative = 0.0, smallest_positive = INFINITY;
////  int i_smallest_negative, i_smallest_positive;
////  
////  for (int i=0; i<4; ++i) {
////    if (d[i] < 0 && d[i] < smallest_negative) {
////      smallest_negative = d[i];
////      i_smallest_negative = i;
////    }
////    if (d[i] > 0 && d[i] < smallest_positive) {
////      smallest_positive = d[i];
////      i_smallest_positive = i;
////    }
////    
////  }
////
////  if (smallest_negative < 0.0) {
////    wall = i_smallest_negative;
////    return smallest_negative;
////  }
////  wall = i_smallest_positive;
////  return smallest_positive;
//}
//
//void Disk::respond_to_collision_with_walls(const Eigen::Vector4f& view_area)
//{
//  // Get current state
//  const Eigen::Vector3d& p = state.p();
//  Eigen::Vector3d& v = state.v();
//  
//  // Current location
//  const double& x = p[0];
//  const double& y = p[1];
//  
//  // Penetrations w.r.t. the four walls
//  double d[4];
//  d[0] = x - radius - view_area[0]; // dl
//  d[1] = view_area[1] - x - radius; // dr
//  d[2] = y - radius - view_area[2]; // db
//  d[3] = view_area[3] - y - radius; // dt
//
//  if (d[0] < 1e-5 && v[0] < 0) v[0] = -v[0];
//  if (d[1] < 1e-5 && v[0] > 0) v[0] = -v[0];
//  if (d[2] < 1e-5 && v[1] < 0) v[1] = -v[1];
//  if (d[3] < 1e-5 && v[1] > 0) v[1] = -v[1];
//}
//
//void Disk::check_collision_disks(const std::vector<Disk*>& disks)
//{
//  Vector3d& p = state.p();
//  
////  std::vector<Disk*>::const_iterator i;
////  for (i = disks.begin(); i != disks.end(); ++i) {
////    Disk* d = *i;
////    Vector3d& p1 = d->s[0];
////    Vector3d n = p1 - p;
////    n.
////    
////  }
//  
//}
