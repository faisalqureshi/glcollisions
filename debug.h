/**
	@file debug.h
	@brief This file provides common debugging macros

  Author: 
  Faisal Zubair
  faisal@cs.toronto.edu

  DATE:
  $Date: 2004/09/07 17:19:29 $

  REVISION:
  $Id: debug.h,v 1.1.1.1 2004/09/07 17:19:29 faisal Exp $


	NOTES:

	 To use dbg levels. define _DBGLEVELS. Then use
   void SetDebugLevel(int), and int GetDebugLevel() to set the desired debug level. Default
   debug level is 0. 
   
   Example:
   -----------------------------------------------------------
   Assume that you called the function TRACEL(x,...)

   Now, it will only print if x <= debug_level. To set debug_level
   Use SetDebugLevel().

   SetDebugLevel(4);
   TRACEL(3,..) ..ok
   TRACEL(4,..) ..ok
   TRACEL(1,..) ..will not print

   -----------------------------------------------------------

	 Also you need to define _CONSOLE if you are doing CONSOLE programming
 

   Common debugging macros.  Many of these macros are similar to those provided
   by MFC and are designed to allow for "MFC neutral" code (code than can be
   compiled with or without MFC support).  Other macros add even more debugging
   facilities.
 
   Description of available macros:
 
   ASSERT			: Identical to the MFC macro of the same name.
   VERIFY			: Identical to the MFC macro of the same name.
   TRACE			: Identical to the MFC macro of the same name.
   INFO				: Similar to TRACE but includes a preamble specifying the
 					      file name and line number where the INFO is called as
 					      well as forces a line break at the end.
   TRACEL			: Identical to the MFC macro of the same name. (use with dbg_level)
   INFOL			: Similar to TRACE but includes a preamble specifying the
 					      file name and line number where the INFO is called as
 					      well as forces a line break at the end. (dbg_level)
	 TRACEOBJ   : Prints class.
	 INFOOBJ    : Prints class. Similar to INFO.
	 TRACEOBJL  : Prints class. (use with dbg_level)
	 INFOOBJL   : Prints class. Similar to INFO. (use with dbg_level)
   DBG				: Code contained within this macro is included only in
 					      _DEBUG builds.
   BREAK			: Forces a break in _DEBUG builds via DebugBreak.
   DECL_DOG_TAG		: Declares a "dog tag" within a class definition (see the
 					          discussion on dog tags below) in _DEBUG builds.
   CHECK_DOG_TAG	: Checks the validity of a "dog tag" in _DEBUG builds.
 
 
   Dog Tags
 
   Dog tags are a technique that can be used to verify that an object has
   not been stepped on by a memory overrun.  To use them you simply use the
   DECL_DOG_TAG macro to declare a dog tag at the beginning and ending of
   the class definition.  After this, you can check the validity of an
   object by using the CHECK_DOG_TAG macro.  For example:
 
   class MyClass
   {
   public:
      DECL_DOG_TAG(tagBegin);
      // code removed for brevity...
      DECL_DOG_TAG(tagEnd);
   };
 
	 and later:
 
   MyClass obj;
   // code removed for brevity...
   CHECK_DOG_TAG(obj.tagBegin);
   CHECK_DOG_TAG(obj.tagEnd);
 
*/

#ifndef __DEBUG_H__
#define __DEBUG_H__

#if defined(WIN32) //WIN32

#if !defined(_CONSOLE) //!_CONSOLE
  #include <afx.h>
#else
  #include <crtdbg.h>
  #include <stdio.h>
  #include <stdarg.h>

#if defined(_DEBUG)
  #define ASSERT _ASSERT
  #define VERIFY(f) ASSERT(f)
#endif
#endif //!_CONSOLE

inline void _cdecl Trace_Func(char* lpszFormat, ...)
{
	va_list args;
	va_start(args, lpszFormat);
  
#if !defined(_CONSOLE) // !_CONSOLE
	int nBuf;
	char szBuffer[512];
  nBuf = _vsnprintf(szBuffer, /*sizeof(szBuffer) / sizeof(char)*/ 512, lpszFormat, args);
  ASSERT(nBuf < sizeof(szBuffer));//Output truncated as it was > sizeof(szBuffer)
  //... here print the string ...
#else // _CONSOLE
	vprintf(lpszFormat,args);
#endif // _CONSOLE
  
	va_end(args);
}

#define TRACE Trace_Func

#endif //WIN32

#ifdef __APPLE__
#if defined(_DEBUG)
#define ASSERT(f) assert(f)
#define VERIFY(f) ASSERT(f)
#endif
#endif

#if defined(_DEBUG)

#define INFO(f)	TRACE("%s (%d):\n", __FILE__, __LINE__); TRACE(f);
#define TRACEOBJ(s, o, m) (s)<<(o)<<(m); (s).flush();
#define INFOOBJ(s, o, m) (s)<<__FILE__<<" ("<<__LINE__<<"):\n"; TRACEOBJ(s,o,m);

class CDogTag
{
public:
	CDogTag() { _this = this; }
	CDogTag(const CDogTag& copy) { _this = this; ASSERT(copy.IsValid()); }
	~CDogTag() { ASSERT(IsValid()); _this = 0; }

	CDogTag& operator=(const CDogTag& rhs)
		{ ASSERT(IsValid() && rhs.IsValid()); return *this; }

	bool IsValid() const { return _this == this; }

private:
   const CDogTag *_this;
};

#define DECL_DOG_TAG(dogTag) CDogTag dogTag;
#define CHECK_DOG_TAG(dogTag) ASSERT((dogTag).IsValid());

#else // !_DEBUG

#define ASSERT ((void) 0)
#define VERIFY ((void) 0)
#define TRACE ((void) 0)
#define INFO ((void) 0)
#define TRACEOBJ ((void) 0)
#define INFOOBJ ((void) 0)

#endif // _DEBUG

// More macros --- make level debugging possible.
#if defined (_DBGLEVELS) && defined(_DEBUG) // _DBGLEVELS && _DEBUG

extern int dbg_level;

inline void _cdecl Trace_Func_Level(int level, char* lpszFormat, ...)
{
	if (level < dbg_level) return;
	
  va_list args;
	va_start(args, lpszFormat);
	int nBuf;
	char szBuffer[512];
  nBuf = _vsnprintf(szBuffer, /*sizeof(szBuffer) / sizeof(char)*/ 512, lpszFormat, args);
  ASSERT(nBuf < sizeof(szBuffer));//Output truncated as it was > sizeof(szBuffer)
  va_end(args);
  TRACE(szBuffer);
}

#define TRACEL Trace_Func_Level
#define INFOL(level,f) TRACEL((level),"%s (%d):\n", __FILE__, __LINE__); TRACEL((level),f);
#define TRACEOBJL(level, s, o, m) if((level) <= dbg_level) (s)<<(o)<<(m); (s).flush();
#define INFOOBJL(level, s, o, m) if ((level) <= dbg_level) (s)<<__FILE__<<" ("<<__LINE__<<"):\n"; TRACEOBJL((level),s,o,m);

extern "C" 
{
extern void SetDebugLevel(int level);
extern int GetDebugLevel();
}

#else // !_DBGLEVELS || !_DEBUG

#define TRACEL (void)0
#define INFOL (void)0
#define TRACEOBJL (void)0
#define INFOOBJL (void)0

#endif // _DBGLEVELS && _DEBUG


#endif // __DEBUG_H__

