//
//  World.h
//  glcollisions
//
//  Created by Faisal Qureshi on 2/16/2014.
//
//

#ifndef glcollisions_World_h
#define glcollisions_World_h

#include <vector>
#include "Eigen/Core"

enum wall {
  top = 0,
  left,
  bottom,
  right
};

enum collision_type {
  wall,
  disk
};

class collision_distance {
public:
  double d;
  int i;
  int j;
  collision_type type;
  bool coll;

  collision_distance(double _d, int _i, int _j, collision_type _type) {
    d = _d;
    i = _i;
    j = _j;
    type = _type;
    coll = false;
  }
  
  collision_distance& operator=(const collision_distance& o) {
    d = o.d;
    i = o.i;
    j = o.j;
    type = o.type;
    coll = o.coll;
    return *this;
  }
};

class Disk;
class DiskState;
class World {
public:
  std::vector<Disk*> disks;
  Eigen::Vector4f area;
  
  double time_cur, time_prev, time_step;
  std::vector<collision_distance> distances;
  bool initializing;
  double initializing_time;
  
public:
  World();
  virtual ~World();
  
  void update_view_area(const Eigen::Vector4f& view_area);
  void populate(int n, const Eigen::Vector4f& view_area);
  void add_disk(Disk* d);
  void add_disk();
  void step(double dt);
  void unstep();
  void render();
  void forward_simulation(double time_step);

  void compute_collision_distances(std::vector<collision_distance>& distances);
  void print_collision_distances(std::vector<collision_distance>& distances);
  int find_smallest_distance(std::vector<collision_distance>& distances, double& smallest_distance);
  void find_time_of_collision(double collision_tolerance, double& smallest_distance);
  void compute_collision_response(double collision_tolerance, std::vector<collision_distance>& distances);
  void print_disk_states();
  int find_next_collision(std::vector<collision_distance>& distances, double& smallest_distance);
  
//  // Collisions
//  void get_smallest(std::vector<collision_distance>& distances,
//                    double& smallest_positive,
//                    double& smallest_negative,
//                    int& i_smallest_positive,
//                    int& i_smallest_negative);
//
//  
//  // Collisions with walls
//  void check_collisions_with_walls();
//  void get_distances_from_walls(std::vector<collision_distance>& distances);
//  void respond_to_collision_with_walls();
//  void goto_collision_instance(double largest_penetration);
//
//  // Inter-disk collisions
//  void get_inter_disk_collision(std::vector<collision_distance>& distances);
};


#endif
